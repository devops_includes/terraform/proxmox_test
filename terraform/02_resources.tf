resource "proxmox_vm_qemu" "terraform" {
//  count = length("${var.vm_name}")
  ipconfig0   = "ip=dhcp"   
  name                      = "${var.vm_name}"
  force_create              = false
  agent                     = 1
  boot                      = "order=scsi0"
  memory                    = 2048
  target_node               = "${var.node}"
  pool                      = "${var.pool}"
  clone                     = "${var.template_name}"
  full_clone                = true
  ciuser                    = "${var.ssh_user}"
  sshkeys                   = "${var.ssh_key}"
  network {
        bridge    = "${var.network_bridge}"
        model     = "virtio"
    }
}

#resource "local_file" "hosts" {
#  content = <<EOF
#[development]
#dev ansible_host=${proxmox_vm_qemu.terraform[0].ssh_host}
#ansible_user=avengerist
#
#EOF
#  filename = "inventory/hosts.inv"
#  provisioner "local-exec" {
#      command = "ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.inv ansible_main.yml"
#  }
#}

