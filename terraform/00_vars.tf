variable "proxmox_url" {
  type = string
  default = ""
}
variable "token_id" {
 type = string
 default = ""
}
variable "token_secret" {
 type = string
 default = ""
}

variable "node" {
  type = string 
  default = ""
}

variable "template_name" {
  type = string
  default = "Ubuntu-Template"
}
variable "vm_name" {
  type = string
  default = "Ubuntu-WP"
}
variable "pool" {
  type = string
  default = "avengerist_pool"
}

variable "ssh_user" {
  type = string
  default = "avengerist"
}

variable "ssh_key" {
  type = string
  default = ""
}

variable "network_bridge" {
  type = string
  default = "vmbr99"
}

