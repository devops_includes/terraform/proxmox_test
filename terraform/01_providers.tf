terraform {
  required_providers {
    proxmox = {
      source  = "Terraform-for-Proxmox/proxmox"
      version = "0.0.1"
    }
  }
}

provider "proxmox" {
  pm_api_url = "${var.proxmox_url}"
  pm_debug = true
  pm_log_enable = true
  pm_api_token_id = "${var.token_id}"
  pm_api_token_secret = "${var.token_secret}"
}

